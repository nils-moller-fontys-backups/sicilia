<div class="inPageMenuLinks">
    <ul class="inPageMenuLinks">
         <li><a href = "#" onclick="loadInfo(this, 'Menu_Starters.php')">Starters</a><br></li>
         <li><a href = "#" onclick="loadInfo(this, 'Menu_Salads.php')">Salads</a><br></li>
         <li><a href = "#" onclick="loadInfo(this, 'Menu_MainDishes.php')">Main dishes</a><br></li>
         <li><a href = "#" onclick="loadInfo(this, 'Menu_Specialties.php')">Specialties</a><br></li>
         <li><a href = "#" onclick="loadInfo(this, 'Menu_Desserts.php')">Desserts</a><br></li>
         <li><a href = "#" onclick="loadInfo(this, 'Menu_Sauces.php')">Sauces</a><br></li>
         <li><a href = "#" onclick="loadInfo(this, 'Menu_Beverages.php')">Beverages</a><br></li>
    </ul>
</div>
<div align="middle" id="menuStartersDiv" class="menuDivs">
    <h1>Menu - starters</h1>
    <table class="menuTables" id="menuStartersTable" border="white">
        <tr>
            <th>
                <img src="pictures\Carpaccio.jpg" alt="Carpaccio picture" width="100" height="100">
                <p><i>Carpaccio</i></p>Thinly sliced beef with lemon juice, cheese, salt and pepper<br>350 gr. €5.99
                <button type="button" onclick="AddItemToCart('Carpaccio','1','5.99')"><img src="pictures\cart.png" width="25" height="25"></button>
            </th>
            <th>
                <img src="pictures\tomato_soup.jpg" alt="Tomato soup picture" width="100" height="90">
                <p><i>Tomato soup</i></p><br>350 gr. €3.95
                <button type="button" onclick="AddItemToCart('Tomato soup','1','3.95')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
        </tr>
        <tr>
            <th>
                <br>
                <img src="pictures/Bruschetta.jpg" alt="Bruschetta picture" width="100" height="100">
                <p><i>Bruschetta</i></p>Warm bread with garlic, oregano, tomato and mozzarella<br>350 gr. €5.95
                <button type="button" onclick="AddItemToCart('Bruschetta','1','5.95')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
            <th>
                <br>
                <img src="pictures/Salsiccia%20alla%20griglia.jpg" alt="Salsiccia alla griglia picture" width="100" height="80">
                <p><i>Salsiccia alla griglia</i></p>Grilled sausage<br>450 gr. €6.80
                <button type="button" onclick="AddItemToCart('Salsiccia alla griglia','1','6.80')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
        </tr>
        <tr>
            <th>
                <br>
                <img src="pictures/Lumache%20al%20forno.jpg" alt="Lumache al forno picture" width="100" height="70">
                <p><i>Lumache al forno</i></p>Baked snails with garlic, parsley and pecorino cheese<br>450 gr. €8.10
                <button type="button" onclick="AddItemToCart('Lumache al forno','1','8.10')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
            <th>
                <br>
                <img src="pictures/Maltagliati.jpg" alt="Maltagliati picture" width="100" height="90">
                <p><i>Maltagliati</i></p>Rough cut type of pasta, Rough cut type of pasta, served with grilled asparagus and sauce from steamed clamps and white wine<br>400 gr. €6.99
                <button type="button" onclick="AddItemToCart('Maltagliati','1','6.99')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
        </tr>
    </table>
</div>