<div class="inPageMenuLinks">
    <ul class="inPageMenuLinks">
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Starters.php')">Starters</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Salads.php')">Salads</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_MainDishes.php')">Main dishes</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Specialties.php')">Specialties</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Desserts.php')">Desserts</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Sauces.php')">Sauces</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Beverages.php')">Beverages</a><br></li>
    </ul>
</div>

<div class="menuDivs" id="menuSpecialtiesDiv">
    <h1>Menu - specialties</h1>
    <table class="menuTables" id="menuSpecialtiesTable" border="white">
        <tr>
            <th>
                <img src="pictures/gelato_diff.jpg" alt="Gelato picture" width="100" height="100">
                <p><i>Gelato</i></p>Different flavours available.<br>150 gr. €5.00
                <button type="button" onclick="AddItemToCart('Gelato','1','5.00')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
            <th>
                <img src="pictures/Gnocchi_di_ricotta_burro_e_salvia.jpg" alt="Gnocchi di ricotta picture" width="100" height="100">
                <p><i>Gnocchi di ricotta</i></p>Dressed in butter and sage.<br>300 gr. €7.10
                <button type="button" onclick="AddItemToCart('Gnocchi di ricotta','1','7.10')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
        </tr>
        <tr>
            <th>
                <img src="pictures/Orecchiette_carbonara.jpg" alt="Orecchiette carbonara picture" width="100" height="100">
                <p><i>Orecchiette carbonara</i></p>300 gr. €9.00
                <button type="button" onclick="AddItemToCart('Orecchiette carbonara','1','9.00')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
            <th>
                <img src="pictures/Lasagne.jpg" alt="Lasagne picture" width="100" height="100">
                <p><i>Lasagne</i></p>150 gr. €8.00
                <button type="button" onclick="AddItemToCart('Lasagne','1','8.00')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
        </tr>
        <tr>
            <th>
                <img src="pictures/Cappon_magro.jpg" alt="Cappon magro picture" width="100" height="100">
                <p><i>Cappon magro</i></p>500 gr. €20.50
                <button type="button" onclick="AddItemToCart('Cappon magro','1','20.50')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
            <th>
                <img src="pictures/Risotto_with_lemon_green_beans.jpg" alt="Risotto with lemon and green beans picture" width="100" height="100">
                <p><i>Risotto with lemon and green beans</i></p>300 gr. €8.50
                <button type="button" onclick="AddItemToCart('Risotto with lemon and green beans','1','8.50')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
        </tr>
    </table>
</div>
