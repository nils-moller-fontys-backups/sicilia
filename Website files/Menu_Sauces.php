<div class="inPageMenuLinks">
    <ul class="inPageMenuLinks">
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Starters.php')">Starters</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Salads.php')">Salads</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_MainDishes.php')">Main dishes</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Specialties.php')">Specialties</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Desserts.php')">Desserts</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Sauces.php')">Sauces</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Beverages.php')">Beverages</a><br></li>
    </ul>
</div>

<div class="menuDivs" id="menuSaucesDiv">
    <h1>Menu - sauces</h1>
    <table class="menuTables" id="menuSaucessTable" border="white">
        <tr>
            <th>
                <img src="pictures/tomato_sauce.jpg" alt="Tomato sauce picture" width="100" height="100">
                <p><i>Tomato sauce</i></p>50 ml €1.00
                <button type="button" onclick="AddItemToCart('Tomato sauce','1','1.00')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
            <th>
                <img src="pictures/barbeque_sauce.jpg" alt="Barbeque sauce picture" width="100" height="100">
                <p><i>Barbeque sauce</i></p>50 ml  €1.00
                <button type="button" onclick="AddItemToCart('Barbeque sauce','1','1.00')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
        </tr>
        <tr>
            <th>
                <img src="pictures/mayo.jpg" alt="Mayonnaise picture" width="100" height="100">
                <p><i>Mayonnaise</i></p>50 ml €0.75
                <button type="button" onclick="AddItemToCart('Mayonnaise','1','0.75')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
            <th>
                <img src="pictures/ketchup.jpg" alt="Ketchup picture" width="100" height="100">
                <p><i>Ketchup</i></p>50ml €0.50
                <button type="button" onclick="AddItemToCart('Ketchup','1','0.50')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
        </tr>
        <tr>
            <th>
                <img src="pictures/Special-Salad-Dressing.jpg" alt="Special salad dressing picture" width="100" height="100">
                <p><i>Special salad dressing</i></p>50 ml €1.20
                <button type="button" onclick="AddItemToCart('Special salad dressing','1','1.20')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
            <th>
                <img src="pictures/spicy_olive_oil.jpg" alt="Spicy olive oil picture" width="100" height="100">
                <p><i>Spicy olive oil</i></p>500 ml €1.10
                <button type="button" onclick="AddItemToCart('Spicy olive oil','1','1.10')"><img src="pictures\cart.png" width="25" height="25">
                </button>
            </th>
        </tr>
    </table>
</div>