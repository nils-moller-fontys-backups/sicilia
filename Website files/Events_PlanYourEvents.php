<div class="inPageEventLinks">
    <a href = "#" onclick="loadInfo(this, 'Events_Planned.php')">Planned Events</a><br>
    <a href = "#" onclick="loadInfo(this, 'Events_PlanYourEvents.php')">Plan your event</a>
</div>
<h1>Plan an event yourself</h1>
<div id="planYourEventPageContent">
    <?php
        session_start();
        include 'PHP/GetCurrentBrowser.php';
        $browser = getBrowser();
        if (isset($_SESSION['Email'])){
            if ($browser == 'Safari'){ //checks for browser
                echo '  <form id="createEventForm" action="PHP/planEventAction.php" method="post">
                        <br>Enter a description of the occasion: <input class="inputBoxesPlaceOwnEvent" title="Your description of the event" type="text" name="inputDescriptionOfEvent"><br><br>
                        Choose a date: <input id="inputDateOfEvent" class="inputBoxesPlaceOwnEvent" title="The date of your event" type="text" name="inputDateOfEvent" placeholder="YYYY-MM-DD"><br><br>
                        Choose a time: <input class="inputBoxesPlaceOwnEvent" type="text" name="inputTimeOfEvent" title="The time of your event" placeholder="HH:MM"><br><br>
                        <div class="btnSectionPlanEvent">
                            <input type="submit" name="btnSubmitEvent" class="submitEventBtn" value="Place event">
                            <input type="reset" name="btnResetForm" class="resetBtn" value="Reset form">
                        </div>
                        </form>';
            }
            else{
                echo '  <form id="createEventForm" action="PHP/planEventAction.php" method="post">
                        <br>Enter a description of the occasion: <input class="inputBoxesPlaceOwnEvent" title="Your description of the event" type="text" name="inputDescriptionOfEvent"><br><br>
                        Choose a date: <input id="inputDateOfEvent" class="inputBoxesPlaceOwnEvent" title="The date of your event" type="date" name="inputDateOfEvent" placeholder="YYYY-MM-DD"><br><br>
                        Choose a time: <input class="inputBoxesPlaceOwnEvent" type="time" name="inputTimeOfEvent" title="The time of your event" placeholder="HH:MM"><br><br>
                        <div class="btnSectionPlanEvent">
                            <input type="submit" name="btnSubmitEvent" class="submitEventBtn" value="Place event">
                            <input type="reset" name="btnResetForm" class="resetBtn" value="Reset form">
                        </div>
                        </form>';
            }
        }
        else if (!isset($_SESSION['Email'])){
            if ($browser == 'Safari') { //checks for browser
                echo '<form id="createEventForm" action="PHP/planEventAction.php" method="post">
                        <br>Enter a description of the occasion: <input class="inputBoxesPlaceOwnEvent" title="Your description of the event" type="text" name="inputDescriptionOfEvent"><br><br>
                        Choose a date: <input id="inputDateOfEvent" class="inputBoxesPlaceOwnEvent" title="The date of your event" type="text" name="inputDateOfEvent" placeholder="YYYY-MM-DD"><br><br>
                        Choose a time: <input class="inputBoxesPlaceOwnEvent" type="text" name="inputTimeOfEvent" title="The time of your event" placeholder="HH:MM"><br><br>
                        Enter your phone number: <input class="inputBoxesPlaceOwnEvent" type="tel" name="inputTelOfPlanningCustomer" placeholder="Contact phone"><br><br>
                        Enter your e-mail: <input class="inputBoxesPlaceOwnEvent" type="email" name="inputEmailOfPlanningCustomer" placeholder="someone.example@gmail.com"><br><br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <div class="btnSectionPlanEvent">
                            <input type="submit" name="btnSubmitEvent" class="submitEventBtn" value="Place event">
                            <input type="reset" name="btnResetForm" class="resetBtn" value="Reset form">
                        </div>
                    </form>';
            }
            else{
                echo '<form id="createEventForm" action="PHP/planEventAction.php" method="post">
                        <br>Enter a description of the occasion: <input class="inputBoxesPlaceOwnEvent" title="Your description of the event" type="text" name="inputDescriptionOfEvent"><br><br>
                        Choose a date: <input id="inputDateOfEvent" class="inputBoxesPlaceOwnEvent" title="The date of your event" type="date" name="inputDateOfEvent"><br><br>
                        Choose a time: <input class="inputBoxesPlaceOwnEvent" type="time" name="inputTimeOfEvent" title="The time of your event"><br><br>
                        Enter your phone number: <input class="inputBoxesPlaceOwnEvent" type="tel" name="inputTelOfPlanningCustomer" placeholder="Contact phone"><br><br>
                        Enter your e-mail: <input class="inputBoxesPlaceOwnEvent" type="email" name="inputEmailOfPlanningCustomer" placeholder="someone.example@gmail.com"><br><br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <div class="btnSectionPlanEvent">
                            <input type="submit" name="btnSubmitEvent" class="submitEventBtn" value="Place event">
                            <input type="reset" name="btnResetForm" class="resetBtn" value="Reset form">
                        </div>
                    </form>';
            }
        }
    ?>
</div>