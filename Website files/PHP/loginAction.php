<!doctype html>
<html>
<head>
    <title>Loggin in</title>
    <link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
<?php
    session_start();
    if (isset($_POST['submit'])){
        if ($_POST['loginEmail'] != "" || $_POST['password'] != ""){
            try{
                $conn = new PDO('mysql:host=studmysql01.fhict.local; dbname=dbi392163', 'dbi392163', 'Touqmoller1');
                $sqlGetStatement = "SELECT Email, Password, FirstName, LastName, PhoneNumber FROM logindata WHERE Email = :email AND Password = :password";
                $stmt = $conn->prepare($sqlGetStatement);
                $inputEmail = $_POST['loginEmail'];
                $inputPassword = $_POST['password'];
                $result = $stmt -> execute(['email' => $inputEmail, 'password' => $inputPassword]);
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                if ($stmt->rowCount() > 0){
                    $_SESSION['Email'] = $row['Email'];
                    $_SESSION['FirstName'] = $row['FirstName'];
                    $_SESSION['LastName'] = $row['LastName'];
                    $_SESSION['Password'] = $row['Password'];
                    $_SESSION['PhoneNumber'] = $row['PhoneNumber'];
                    echo "<script>
                    alert('Welcome " . $row['FirstName'] . "!');
                    window.location.href='../index.php';
                    </script>";
                }
                else{
                    echo "<script>
                        alert('No combination of that email and password exist. Did you make a mistake? If you do not have an account yet, sign up!');
                        window.location.href='../index.php';
                        </script>";
                }
            }
            catch (PDOException $e){
                echo "<script>
                        alert('Database error');
                        window.location.href='../index.php';
                        </script>";
            }
            finally{
                $conn = null;
            }
        }
        else{
            echo "<script>
                        alert('Please fill in all fields');
                        window.location.href='../index.php';
                        </script>";
        }
    }
?>
</body>
</html>